@extends('layouts.app')
@section('content')
                    <h1> Edit candidate </h1>
                    <form method = "post" action = "{{action('CandidatesController@update', $candidate->id)}}">
                    @csrf
                    @method('PATCH')
                    <div>
                        <label for = "name">Candidate name</label>
                        <input type = "text" name = "name" value = {{$candidate->name}}>
                    </div>
                    <div>
                        <label for = "email">Candidate email</label>
                        <input type = "text" name = "email" value = {{$candidate->email}}>
                    </div>
                    <div>
                        <input type = "submit" name = "submit" value = "Update candidate">
                    </div>

                    </form>
@endsection