@extends('layouts.app')
@section('content')
                    <h1> Create candidate </h1>
                    <form method = "post" action = "{{action('CandidatesController@store')}}">
                    @csrf
                    <div>
                        <label for = "name">Candidate name</label>
                        <input type = "text" name = "name">
                    </div>
                    <div>
                        <label for = "email">Candidate email</label>
                        <input type = "text" name = "email">
                    </div>
                    <div>
                        <input type = "submit" name = "submit" value = "Create candidate">
                    </div>

                    </form>
@endsection