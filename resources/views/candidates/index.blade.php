@extends('layouts.app')
@section('content')
    <p>
    <h4 class="text-center"> List of candidates</h4>
    </p>
    <table class = 'table'>
        <tr>
            <th>id</th><th>Name</th><th>Email</th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th>
        </tr> 
        <!-- the tabledata -->
        @foreach($candidates as $candidate) 
            <tr>
                <td>{{$candidate->id}}</td>
                <td>{{$candidate->name}}</td>
                <td>{{$candidate->email}}</td>
                <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(isset($candidate->user_id))
                        {{$candidate->owner->name}}
                    @else
                        Assign Owner
                    @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($users as $user)
                        <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id, $user->id])}}">{{$user->name}}</a>
                    @endforeach
                    </div>
                </div>
                </td>
                <td>
                <div class="dropdown">
                    @if(App\Status::next($candidate->status_id) != null)
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if (isset($candidate->status_id))
                        {{$candidate->status->name}}
                    @else
                        Define Status
                    @endif
                    </button>
                    @else
                        {{$candidate->status->name}}
                    @endif


                    @if(App\Status::next($candidate->status_id) != null)
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach(App\Status::next($candidate->status_id) as $status)
                        <a class="dropdown-item" href="{{route('candidate.changestatus',[$candidate->id, $status->id])}}">{{$status->name}}</a>
                    @endforeach
                    </div>
                    @endif
                </div>
                </td>
                <td>{{$candidate->created_at}}</td>
                <td>{{$candidate->updated_at}}</td>
                <td><a href="{{action('CandidatesController@edit',$candidate->id)}}">Edit</a></td>
                <td><a href="{{action('CandidatesController@destroy',$candidate->id)}}">Delete</a></td>
            </tr>
        @endforeach  
    </table>
@endsection