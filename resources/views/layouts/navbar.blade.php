<nav class="navbar navbar-expand-lg navbar navbar-light" style="background-color: #e3f2fd;">
<a class="navbar-brand" href="#">Candidates Site |</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" href="{{url('/candidates')}}">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" {{url('/candidates/create')}}">Create New Candidate</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=" {{url('/candidates')}}">My Candidates</a>
      </li>
    </ul>
   
  </div>
</nav>