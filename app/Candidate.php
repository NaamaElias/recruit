<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Candidate extends Model
{
    protected $fillable = ['name','email'];
    
    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status(){
        return $this->belongsTo('App\Status');
    }

   
}
