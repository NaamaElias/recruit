<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/hello', function (){
    return 'Hello Laravel';
});


Route::get('/student/{id?}', function ($id = 'Student Not Found'){
    return 'We got student with id '.$id;
});


Route::get('/car/{id?}', function ($id = null){
    if(isset($id)){
        return "We got car $id";
    } else {
        return "We need the id in order to find your car";
    }
});

Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});


//EX5 : the user need to assign the email (required), and the name is optional.

Route::get('/users/{email}/{name?}', function ($email, $name = 'name missing'){
        return view('users', compact('name','email'));
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');

Route::get('candidates/delete/{id}','CandidatesController@destroy')->name('candidate.delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('candidates/changeuser/{cid}/{uid?}','CandidatesController@changeUser')->name('candidate.changeuser');

Route::get('candidates/changestatus/{cid}/{sid}','CandidatesController@changeStatus')->name('candidate.changestatus');

?>